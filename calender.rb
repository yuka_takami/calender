require 'webrick'
require 'dbi'
require 'erb'
require 'date'

config = {
    :Port => 8080,
    :DocumentRoot => '.'
}

server = WEBrick::HTTPServer.new( config )

#DBにデータ登録
server.mount_proc "/calender" do |req, res|
  @calc = []
  dbh = DBI.connect( 'DBI:SQLite3:calender.db' )
  dbh.do("create table if not exists calender_tbl 
  ( id integer primary key autoincrement, date varchar(50), time varchar(50), memo varchar(100))")
  dbh.select_all("select * from calender_tbl") do |row|
    @calc << {id: row["id"] ,date: row["date"] ,time: row["time"], memo: row["memo"]} 
  end
  dbh.disconnect
   res.body = ERB.new(File.read('list.erb')).result(binding)
end

#DBのデータを表示
server.mount_proc "/calender/regist" do |req, res|
  dbh = DBI.connect( 'DBI:SQLite3:calender.db' )
  dbh.do("insert into calender_tbl (date,time,memo) values (\'#{req.query["date"]}\',\'#{req.query["time"]}\',\'#{req.query["memo"]}\')")
  dbh.disconnect
  res.set_redirect(WEBrick::HTTPStatus::TemporaryRedirect, '/calender')
end

#カレンダーの内容を表示
server.mount_proc "/calender/date" do |req, res|
   @cal = []
   @flag = false
  dbh = DBI.connect( 'DBI:SQLite3:calender.db' )
  dbh.select_all("select * from calender_tbl where date = '#{req.query["date"]}' ") do |row|
    @cal << {id: row["id"] ,date: row["date"] ,time: row["time"], memo: row["memo"]} 
    @flag = true
  end
  res.body = ERB.new(File.read('date.erb')).result(binding)
end

#DBのデータを削除
server.mount_proc "/calender/delete" do |req, res|
  dbh = DBI.connect( 'DBI:SQLite3:calender.db' )
  dbh.do("delete from calender_tbl where id=#{req.query["id"]}")
  dbh.disconnect
  @cal = []
  @flag = false
  dbh = DBI.connect( 'DBI:SQLite3:calender.db' )
  dbh.select_all("select * from calender_tbl where date = '#{req.query["date"]}' ") do |row|
    @cal << {id: row["id"] ,date: row["date"] ,time: row["time"], memo: row["memo"]} 
    @flag = true
  end
  res.body = ERB.new(File.read('date.erb')).result(binding)
end

#DBのデータを編集
server.mount_proc "/calender/edit" do |req, res|
  id = req.query["id"]  #edit.erbに渡すため
  date = ""
  time = ""
  memo = "" #edit.erbに渡すため（これは大事；スコープ）
  dbh = DBI.connect( 'DBI:SQLite3:calender.db' )
  dbh.select_all("select date, time, memo from calender_tbl where id=#{id}") do |row|
    date = row[0]
    time = row[1]
    memo = row[2]
  end
  res.body = ERB.new(File.read('edit.erb')).result(binding)
end

#DBのデータを更新
server.mount_proc "/calender/update" do |req, res|
  dbh = DBI.connect( 'DBI:SQLite3:calender.db' )
  dbh.do("update calender_tbl set 
   date=\'#{req.query["date"]}\', time=\'#{req.query["time"]}\',memo=\'#{req.query["memo"]}\' where id=#{req.query["id"]}")
  dbh.disconnect
  res.set_redirect(WEBrick::HTTPStatus::TemporaryRedirect, '/calender')
end

trap(:INT) do
    server.shutdown
end

server.start